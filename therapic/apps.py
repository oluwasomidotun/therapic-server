from django.apps import AppConfig


class TherapicConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'therapic'
